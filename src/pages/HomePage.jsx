import PrimarySearchAppBar from '../components/PrimarySearchAppBar';
import LabelBottomNavigation from '../components/LabelBottomNavigation';
import BasicTable from '../components/BasicTable';
import Typography from '@mui/material/Typography';

const HomePage = () => {

  return (
    <>
      <PrimarySearchAppBar />
      <Typography variant="h1" component="h2">
        List of repositories
      </Typography>
      <BasicTable isFavorite={false} />
      <LabelBottomNavigation />
    </>
  )
}

export default HomePage;