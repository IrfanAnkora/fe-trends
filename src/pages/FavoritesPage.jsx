import PrimarySearchAppBar from '../components/PrimarySearchAppBar';
import LabelBottomNavigation from '../components/LabelBottomNavigation';
import BasicTable from '../components/BasicTable';
import Typography from '@mui/material/Typography';

const FavoritesPage = () => {

  return (
    <>
      <PrimarySearchAppBar />
      <Typography variant="h1" component="h2">
        List of favorite repositories
      </Typography>
      <BasicTable isFavorite={true} />
      <LabelBottomNavigation />
    </>
    )
}

export default FavoritesPage;