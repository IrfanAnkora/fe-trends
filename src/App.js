import { Routes, Route, BrowserRouter } from "react-router-dom"
import HomePage from './pages/HomePage';
import FavoritesPage from './pages/FavoritesPage';

import './App.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';


function App() {
 return (
   <div className="App">
     <BrowserRouter>
       <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/favorites" element={<FavoritesPage />} />
       </Routes>
     </BrowserRouter>
   </div>
 )
}

export default App
