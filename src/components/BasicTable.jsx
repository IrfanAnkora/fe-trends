import React, { useEffect, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

import {StarOutline, Grade} from '@mui/icons-material';

function createData(name, link, description, stars, id) {
  return { name, link, description, stars, id };
}

export default function BasicTable({isFavorite}) {

  const [items, setItems] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);

  const rows = items.map(i => {
    return createData(i.name, i.git_url, i.description, i.stargazers_count, i.id);
  })

  const handleFavoriteClick = (id) => {
    if (selectedItems.includes(id)) {
      // Remove the project from selectedItems if already selected
      setSelectedItems(selectedItems.filter((i) => i !== id));
    
      fetch(`http://localhost:4000/repositories/favorite/${id}`, {
        method: "DELETE",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
      })
      .then(response => response.json())
      .then(json => console.log(json));
    } else {
      // Add the project to selectedItems if not already selected
      setSelectedItems([...selectedItems, id]);

      fetch(`http://localhost:4000/repositories/favorite/${id}?q=tetris+language:assembly&sort=stars&order=desc&per_page=100&page=1`, {
        method: "POST",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
      })
      .then(response => response.json())
      .then(json => console.log(json));
    }
  };

  useEffect(() => {
    const url = isFavorite ? "http://localhost:4000/repositories/favorites" : "http://localhost:4000/repositories?q=tetris+language:assembly&sort=stars&order=desc&per_page=2&page=1";
      
      fetch(url)
      .then(response => response.json())
      .then(data => isFavorite ? setItems(data) : setItems(data.items))
  }, [isFavorite]);

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Repo name</TableCell>
            <TableCell align="left">Link</TableCell>
            <TableCell align="left">Description</TableCell>
            <TableCell align="left">Stars</TableCell>
            <TableCell align="left">Favorite</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {rows.length < 1 ? (
            <TableRow>
              <TableCell component="th" scope="row">
                <Typography variant="h3" component="h3">
                  There are no reporitories to display
                </Typography>
              </TableCell>
            </TableRow>
          ) :
          rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="left">{row.link}</TableCell>
              <TableCell align="left">{row.description}</TableCell>
              <TableCell align="left">{row.stars}</TableCell>
              <TableCell align="left">
                {isFavorite ? (<span><Grade /></span>) : 
                  selectedItems.includes(row.id) ? (
                    <span onClick={() => handleFavoriteClick(row.id)}><Grade /></span>  // Render selected icon
                  ) : (
                    <span onClick={() => handleFavoriteClick(row.id)}><StarOutline /></span> // Render favorite icon
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}